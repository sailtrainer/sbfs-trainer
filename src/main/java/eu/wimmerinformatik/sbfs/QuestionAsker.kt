/*  Copyright 2014 - 2022 Matthias Wimmer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package eu.wimmerinformatik.sbfs

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import eu.wimmerinformatik.sbfs.data.Repository
import java.text.DateFormat
import java.util.*

/**
 * @author matthias
 */
class QuestionAsker : Activity() {
    private var repository: Repository? = null
    private var currentQuestion = 0
    private var topicId = 0
    private var correctChoice = 0
    private var maxProgress = 0
    private var currentProgress = 0
    private var rand: Random? = Random()
    private val order: MutableList<Int> = mutableListOf()
    private var showingCorrectAnswer = false
    private var nextTime: Date? = null
    private var waitTimer: Timer? = null
    private var showingStandardView = false
    private val replaceNNBSP =
        Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH

    public override fun onDestroy() {
        super.onDestroy()
        cancelTimer()
        repository!!.close()
        repository = null
        rand = null
        order.clear()
        nextTime = null
    }

    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(
            javaClass.name + ".showingCorrectAnswer",
            showingCorrectAnswer
        )
        outState.putInt(javaClass.name + ".currentQuestion", currentQuestion)
        outState.putInt(javaClass.name + ".maxProgress", maxProgress)
        outState.putInt(javaClass.name + ".currentProgress", currentProgress)
        outState.putLong(javaClass.name + ".topic", topicId.toLong())
        if (nextTime != null) {
            outState.putLong(javaClass.name + ".nextTime", nextTime!!.time)
        }
        if (order.isNotEmpty()) {
            val orderString = order.joinToString(",")
            outState.putString(
                javaClass.name + ".order",
                orderString
            )
        }
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        repository = Repository(this)
        showStandardView()
        if (savedInstanceState != null) {
            topicId = savedInstanceState.getLong(javaClass.name + ".topic")
                .toInt()
            currentQuestion =
                savedInstanceState.getInt(javaClass.name + ".currentQuestion")
            val nextTimeLong =
                savedInstanceState.getLong(javaClass.name + ".nextTime")
            nextTime = if (nextTimeLong > 0L) Date(nextTimeLong) else null
            showingCorrectAnswer =
                savedInstanceState.getBoolean(javaClass.name + ".showingCorrectAnswer")
            maxProgress =
                savedInstanceState.getInt(javaClass.name + ".maxProgress")
            currentProgress =
                savedInstanceState.getInt(javaClass.name + ".currentProgress")
            val orderString =
                savedInstanceState.getString(javaClass.name + ".order")
            if (orderString != null) {
                val orderArray = orderString.split(",").toTypedArray()
                order.clear()
                for (s in orderArray) {
                    order.add(s.toInt())
                }
            }
            showQuestion()
        } else {
            topicId = intent.extras!!.getLong(javaClass.name + ".topic")
                .toInt()
            nextQuestion()
        }
    }

    /**
     * Populate the options menu.
     *
     * @param menu the menu to populate
     * @return always true
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.askermenu, menu)
        return true
    }

    /**
     * Handle option menu selections.
     *
     * @param item the Item the user selected
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle item selection
        return when (item.itemId) {
            R.id.resetTopic -> {
                askRestartTopic()
                true
            }
            R.id.statistics -> {
                val intent = Intent(this, StatisticsActivity::class.java)
                intent.putExtra(
                    StatisticsActivity::class.java.name + ".topic",
                    topicId
                )
                startActivity(intent)
                true
            }
            R.id.help -> {
                val intent2 = Intent(Intent.ACTION_VIEW)
                val uri =
                    "http://sportboot.mobi/trainer/segeln/sbfs/app/help?question=" +
                            currentQuestion +
                            "&topic=" +
                            topicId +
                            "&view=QuestionAsker"
                intent2.data = Uri.parse(uri)
                startActivity(intent2)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Restarts the topic after asking for confirmation.
     */
    private fun askRestartTopic() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(R.string.warningReset)
        builder.setCancelable(true)
        builder.setPositiveButton(R.string.resetOkay) { _: DialogInterface?, _: Int -> restartTopic() }
        builder.setNegativeButton(R.string.resetCancel) { _: DialogInterface?, _: Int -> }
        val alert = builder.create()
        alert.show()
    }

    private fun restartTopic() {
        repository!!.resetTopic(topicId)
        nextQuestion()
    }

    @SuppressLint("MissingInflatedId")
    private fun showStandardView() {
        setContentView(R.layout.question_asker)
        showingStandardView = true
        val progress = findViewById<ProgressBar>(R.id.progressBar1)
        progress.max = 5
        val radioGroup = findViewById<RadioGroup>(R.id.radioGroup1)
        val contButton = findViewById<Button>(R.id.button1)

        // only enable continue when answer is selected
        radioGroup.setOnCheckedChangeListener { _: RadioGroup?, _: Int ->
            contButton.isEnabled = radioGroup.checkedRadioButtonId != -1
        }

        // @Override
        contButton.setOnClickListener {

            // find what has been selected
            if (showingCorrectAnswer) {
                showingCorrectAnswer = false
                radioGroup.isEnabled = true
                nextQuestion()
                return@setOnClickListener
            }
            val selectedButton = radioGroup.checkedRadioButtonId
            when {
                selectedButton == correctChoice -> {
                    Toast.makeText(
                        this@QuestionAsker,
                        getString(R.string.right),
                        Toast.LENGTH_SHORT
                    ).show()
                    repository!!.answeredCorrect(currentQuestion)
                    nextQuestion()
                }
                selectedButton != -1 -> {
                    repository!!.answeredIncorrect(currentQuestion)
                    showingCorrectAnswer = true
                    radioGroup.isEnabled = false
                    val correctButton = findViewById<RadioButton>(correctChoice)
                    correctButton.setBackgroundResource(R.color.correctAnswer)
                    correctButton.setTextAppearance(
                        this@QuestionAsker,
                        R.style.correctAnswerStyle
                    )
                }
                else -> {
                    Toast.makeText(
                        this@QuestionAsker,
                        getString(R.string.noAnswerSelected),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    @SuppressLint("MissingInflatedId")
    private fun nextQuestion() {
        if (!showingStandardView) {
            showStandardView()
        }
        if (correctChoice != 0) {
            val correctButton = findViewById<RadioButton>(correctChoice)
            correctButton.setBackgroundResource(0)
            correctButton.setTextAppearance(
                this,
                android.R.style.TextAppearance
            )
        }
        order.clear()
        val radioGroup = findViewById<RadioGroup>(R.id.radioGroup1)
        radioGroup.clearCheck()
        val contButton = findViewById<Button>(R.id.button1)
        contButton.isEnabled = false
        val nextQuestion = repository!!.selectQuestion(topicId)

        // any question?
        val selectedQuestion = nextQuestion.selectedQuestion
        if (selectedQuestion != 0) {
            currentQuestion = selectedQuestion
            maxProgress = nextQuestion.maxProgress
            currentProgress = nextQuestion.currentProgress
            nextTime = null
            showQuestion()
            return
        }
        nextTime = nextQuestion.nextQuestion
        if (nextTime != null) {
            showQuestion()
            return
        }
        showingStandardView = false
        setContentView(R.layout.no_more_questions_finished)
        val restartTopicButton = findViewById<Button>(R.id.restartTopic)
        //@Override
        restartTopicButton.setOnClickListener {
            restartTopic()
            nextQuestion()
        }
    }

    private val radioButtons: List<RadioButton>
        get() {
            val radioButtons: MutableList<RadioButton> = LinkedList()
            radioButtons.add(findViewById(R.id.radio0))
            radioButtons.add(findViewById(R.id.radio1))
            radioButtons.add(findViewById(R.id.radio2))
            radioButtons.add(findViewById(R.id.radio3))
            return radioButtons
        }

    private fun showQuestion() {
        if (nextTime != null) {
            showingStandardView = false
            setContentView(R.layout.no_more_questions_wait)
            val nextTimeText = findViewById<TextView>(R.id.nextTimeText)
            if (nextTime!!.time - Date().time < 64800000L) {
                nextTimeText.text =
                    DateFormat.getTimeInstance().format(nextTime!!)
            } else {
                nextTimeText.text =
                    DateFormat.getDateTimeInstance().format(nextTime!!)
            }
            showNextQuestionAt(nextTime!!)
            val resetWaitButton = findViewById<Button>(R.id.resetWait)
            resetWaitButton.setOnClickListener {
                cancelTimer()
                repository!!.continueNow(topicId)
                nextQuestion()
            }
            return
        }
        val question = repository!!.getQuestion(currentQuestion)
        val levelText = findViewById<TextView>(R.id.levelText)
        levelText.text =
            when {
                question!!.level == 0 -> getString(R.string.firstPass)
                question.level == 1 -> getString(
                    R.string.secondPass
                )
                question.level == 2 -> getString(R.string.thirdPass)
                question.level == 3 -> getString(
                    R.string.fourthPass
                )
                question.level == 4 -> getString(R.string.fifthPass)
                else -> String.format(
                    getString(R.string.passText),
                    question.level
                )
            }
        val textView = findViewById<TextView>(R.id.textViewFrage)
        textView.text = safeText(question.questionText)
        val radioButtons = radioButtons
        if (order.isEmpty()) {
            for (i in 0..3) {
                order.add(rand!!.nextInt(order.size + 1), i)
            }
        }
        correctChoice = radioButtons[order[0]].id
        for (i in 0..3) {
            radioButtons[order[i]].text = safeText(question.answers[i])
        }
        val progressBar = findViewById<ProgressBar>(R.id.progressBar1)
        progressBar.max = maxProgress
        progressBar.progress = currentProgress

        // remove previous question image if any
        val linearLayout = findViewById<LinearLayout>(R.id.linearLayout1)
        for (i in 0 until linearLayout.childCount) {
            val childAtIndex = linearLayout.getChildAt(i)
            if (childAtIndex is ImageView) {
                linearLayout.removeViewAt(i)
                break
            }
        }
        val questionImage = questionImage
        if (questionImage != null) {
            linearLayout.addView(questionImage, 2)
        }
    }

    private val questionImage: ImageView?
        get() {
            val imageResourceId: Int = when (currentQuestion) {
                9605 -> R.drawable.q17
                9606 -> R.drawable.q18
                9607 -> R.drawable.q19
                9608 -> R.drawable.q20
                9609 -> R.drawable.q21
                9610 -> R.drawable.q22
                9611 -> R.drawable.q23
                9612 -> R.drawable.q24
                9613 -> R.drawable.q25
                9614 -> R.drawable.q26
                9615 -> R.drawable.q27
                9616 -> R.drawable.q28
                9617 -> R.drawable.q29
                9618 -> R.drawable.q30
                9680 -> R.drawable.q91
                9681 -> R.drawable.q92
                9682 -> R.drawable.q93
                9683 -> R.drawable.q94
                9686 -> R.drawable.q97
                9687 -> R.drawable.q98
                9688 -> R.drawable.q99
                9691 -> R.drawable.q102
                9692 -> R.drawable.q103
                9693 -> R.drawable.q104
                9694 -> R.drawable.q105
                9695 -> R.drawable.q106
                9696 -> R.drawable.q107
                9697 -> R.drawable.q108
                9698 -> R.drawable.q109
                9699 -> R.drawable.q110
                9700 -> R.drawable.q111
                9701 -> R.drawable.q112
                9704 -> R.drawable.q115
                9711 -> R.drawable.q122
                9712 -> R.drawable.q123
                9722 -> R.drawable.q133
                9723 -> R.drawable.q134
                9737 -> R.drawable.q148
                9738 -> R.drawable.q149
                9739 -> R.drawable.q150
                9740 -> R.drawable.q151
                9743 -> R.drawable.q154
                9765 -> R.drawable.q176
                9766 -> R.drawable.q177
                9768 -> R.drawable.q179
                9769 -> R.drawable.q180
                9771 -> R.drawable.q182
                9773 -> R.drawable.q184
                9774 -> R.drawable.q185
                9776 -> R.drawable.q187
                9778 -> R.drawable.q189
                9779 -> R.drawable.q190
                9780 -> R.drawable.q191
                9781 -> R.drawable.q192
                9782 -> R.drawable.q193
                9783 -> R.drawable.q194
                9784 -> R.drawable.q195
                9785 -> R.drawable.q196
                9789 -> R.drawable.q200
                9790 -> R.drawable.q201
                9791 -> R.drawable.q202
                9792 -> R.drawable.q203
                9793 -> R.drawable.q204
                9794 -> R.drawable.q205
                9795 -> R.drawable.q206
                9796 -> R.drawable.q207
                9797 -> R.drawable.q208
                9849 -> R.drawable.q260
                9853 -> R.drawable.q265
                9854 -> R.drawable.q266
                9872 -> R.drawable.q284
                9873 -> R.drawable.q285
                else -> return null
            }
            val image = ImageView(this)
            image.setImageResource(imageResourceId)
            return image
        }

    private fun showNextQuestionAt(`when`: Date) {
        scheduleTask(object : TimerTask() {
            override fun run() {
                runOnUiThread { nextQuestion() }
            }
        }, `when`)
    }

    @Synchronized
    private fun scheduleTask(task: TimerTask, `when`: Date) {
        cancelTimer()
        waitTimer = Timer("waitNextQuestion", true)
        waitTimer!!.schedule(task, `when`)
    }

    @Synchronized
    private fun cancelTimer() {
        if (waitTimer != null) {
            waitTimer!!.cancel()
            waitTimer = null
        }
    }

    private fun safeText(source: String?): String {
        return if (replaceNNBSP && source != null) source.replace(
            '\u202f',
            '\u00a0'
        ) else source!!
    }
}