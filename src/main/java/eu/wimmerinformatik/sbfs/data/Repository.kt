/*  Copyright 2014 - 2022 Matthias Wimmer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package eu.wimmerinformatik.sbfs.data

import android.content.Context
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteDatabase
import eu.wimmerinformatik.sbfs.R
import org.xmlpull.v1.XmlPullParser
import java.io.IOException
import org.xmlpull.v1.XmlPullParserException
import android.database.Cursor
import android.content.ContentValues
import android.widget.SimpleCursorAdapter
import java.util.*

class Repository(private val context: Context) :
    SQLiteOpenHelper(context, "topics", null, 8) {
    private var answerIdSeq = 0
    private var database: SQLiteDatabase? = null
    private val done: String = context.getString(R.string.done)
    override fun onCreate(db: SQLiteDatabase) {
        // create databases
        db.beginTransaction()
        try {
            db.execSQL("CREATE TABLE topic (_id INT NOT NULL PRIMARY KEY, order_index INT NOT NULL UNIQUE, name TEXT NOT NULL)")
            db.execSQL("CREATE TABLE question (_id INT NOT NULL PRIMARY KEY, topic_id INT NOT NULL REFERENCES topic(_id) ON DELETE CASCADE, reference TEXT, question TEXT NOT NULL, level INT NOT NULL, next_time INT NOT NULL)")
            db.execSQL("CREATE TABLE answer (_id INT NOT NULL PRIMARY KEY, question_id INT NOT NULL REFERENCES question(_id) ON DELETE CASCADE, order_index INT NOT NULL, answer TEXT NOT NULL)")
            db.setTransactionSuccessful()
        } finally {
            db.endTransaction()
        }

        // fill with data
        try {
            val topics: MutableList<Topic?> = LinkedList()
            val questions: MutableList<Question?> = LinkedList()
            val xmlResourceParser = context.resources.getXml(R.xml.sbfsfragen)
            var eventType = xmlResourceParser.eventType
            var currentTopic: Topic? = null
            var currentQuestion: Question? = null
            var expectingAnswer = false
            var expectingQuestion = false
            var index = 0
            while (eventType != XmlPullParser.END_DOCUMENT) {
                when (eventType) {
                    XmlPullParser.START_TAG -> {
                        when (xmlResourceParser.name) {
                            "topic" -> {
                                currentTopic = Topic()
                                currentTopic.id =
                                    xmlResourceParser.getAttributeIntValue(
                                        null,
                                        "id",
                                        0
                                    )
                                currentTopic.index = index++
                                currentTopic.name =
                                    xmlResourceParser.getAttributeValue(
                                        null,
                                        "name"
                                    )
                            }
                            "question" -> {
                                currentQuestion = Question()
                                currentQuestion.id =
                                    xmlResourceParser.getAttributeIntValue(
                                        null,
                                        "id",
                                        0
                                    )
                                currentQuestion.reference =
                                    xmlResourceParser.getAttributeValue(
                                        null,
                                        "reference"
                                    )
                                currentQuestion.nextTime = Date()
                                currentQuestion.topicId = currentTopic!!.id
                            }
                            "text" -> {
                                expectingQuestion = true
                            }
                            "correct" -> {
                                expectingAnswer = true
                            }
                            "incorrect" -> {
                                expectingAnswer = true
                            }
                        }
                    }
                    XmlPullParser.TEXT -> {
                        if (expectingQuestion) {
                            currentQuestion!!.questionText =
                                xmlResourceParser.text
                            expectingQuestion = false
                        }
                        if (expectingAnswer) {
                            currentQuestion!!.answers.add(xmlResourceParser.text)
                            expectingAnswer = false
                        }
                        val endTagName = xmlResourceParser.name
                        if ("topic" == endTagName) {
                            topics.add(currentTopic)
                            currentTopic = null
                        } else if ("question" == endTagName) {
                            questions.add(currentQuestion)
                            currentQuestion = null
                        }
                    }
                    XmlPullParser.END_TAG -> {
                        val endTagName = xmlResourceParser.name
                        if ("topic" == endTagName) {
                            topics.add(currentTopic)
                            currentTopic = null
                        } else if ("question" == endTagName) {
                            questions.add(currentQuestion)
                            currentQuestion = null
                        }
                    }
                }
                xmlResourceParser.next()
                eventType = xmlResourceParser.eventType
            }
            xmlResourceParser.close()
            db.beginTransaction()
            try {
                for (topic in topics) {
                    save(db, topic)
                }
                for (question in questions) {
                    save(db, question)
                }
                db.setTransactionSuccessful()
            } finally {
                db.endTransaction()
            }
        } catch (ioe: IOException) {
            ioe.printStackTrace()
        } catch (e: XmlPullParserException) {
            e.printStackTrace()
        }
    }

    fun selectQuestion(topicId: Int): QuestionSelection {
        val result = QuestionSelection()
        val possibleQuestions: MutableList<Int> = LinkedList()
        val now = Date().time
        var questionCount = 0
        var maxProgress = 0
        var currentProgress = 0
        var soonestNextTime: Long = 0
        db!!.query(
            "question",
            arrayOf("_id", "level", "next_time"),
            "topic_id=?",
            arrayOf(
                topicId.toString()
            ),
            null,
            null,
            null,
            null
        )
            .use { c ->
                c.moveToNext()
                while (!c.isAfterLast) {
                    val questionId = c.getInt(0)
                    val level = c.getInt(1)
                    val nextTime = c.getLong(2)
                    questionCount++
                    maxProgress += NUMBER_LEVELS
                    currentProgress += level
                    if (level < NUMBER_LEVELS) {
                        if (nextTime > now) {
                            if (soonestNextTime == 0L || soonestNextTime > nextTime) {
                                soonestNextTime = nextTime
                            }
                        } else {
                            possibleQuestions.add(questionId)
                        }
                    }
                    c.moveToNext()
                }
            }
        result.totalQuestions = questionCount
        result.maxProgress = maxProgress
        result.currentProgress = currentProgress
        result.isFinished = possibleQuestions.isEmpty() && soonestNextTime == 0L
        if (possibleQuestions.isNotEmpty()) {
            val rand = Random()
            result.selectedQuestion =
                possibleQuestions[rand.nextInt(possibleQuestions.size)]
        } else if (soonestNextTime > 0) {
            result.nextQuestion = Date(soonestNextTime)
        }
        return result
    }

    fun getQuestion(questionId: Int): Question? {
        val question = Question()
        db!!.query(
            "question",
            arrayOf(
                "_id",
                "topic_id",
                "reference",
                "question",
                "level",
                "next_time"
            ),
            "_id=?",
            arrayOf(
                questionId.toString()
            ),
            null,
            null,
            null,
            null
        )
            .use { c ->
                c.moveToNext()
                if (c.isAfterLast) {
                    return null
                }
                question.id = c.getInt(0)
                question.topicId = c.getInt(1)
                question.reference = c.getString(2)
                question.questionText = c.getString(3)
                question.level = c.getInt(4)
                question.nextTime = Date(c.getLong(5))
            }

        // _id INT NOT NULL PRIMARY KEY, question_id INT NOT NULL REFERENCES question(id) ON DELETE CASCADE, order_index INT NOT NULL, answer TEXT
        db!!.query(
            "answer", arrayOf("answer"), "question_id=?", arrayOf(
                questionId.toString()
            ), null, null, "order_index"
        )
            .use { answer ->
                answer.moveToNext()
                while (!answer.isAfterLast) {
                    question.answers.add(answer.getString(0))
                    answer.moveToNext()
                }
            }
        return question
    }

    fun getTopic(topicId: Int): Topic? {
        val topic = Topic()
        db!!.query(
            "topic", arrayOf("_id", "order_index", "name"), "_id=?", arrayOf(
                topicId.toString()
            ), null, null, null
        )
            .use { c ->
                c.moveToNext()
                if (c.isAfterLast) {
                    return null
                }
                topic.id = c.getInt(0)
                topic.index = c.getInt(1)
                topic.name = c.getString(2)
            }
        return topic
    }

    fun getTopicStat(topicId: Int): TopicStats {
        val stats = TopicStats()
        stats.levels = NUMBER_LEVELS
        stats.questionsAtLevel = IntArray(NUMBER_LEVELS + 1)
        var currentProgress = 0
        var maxProgress = 0
        var questionCount = 0
        db!!.query(
            "question", arrayOf("_id", "level"), "topic_id=?", arrayOf(
                topicId.toString()
            ), null, null, null, null
        )
            .use { c ->
                c.moveToNext()
                while (!c.isAfterLast) {
                    questionCount++
                    currentProgress += c.getInt(1)
                    maxProgress += NUMBER_LEVELS
                    stats.questionsAtLevel[c.getInt(1)]++
                    c.moveToNext()
                }
            }
        stats.currentProgress = currentProgress
        stats.maxProgress = maxProgress
        stats.questionCount = questionCount
        return stats
    }

    fun answeredCorrect(questionId: Int) {
        val question = getQuestion(questionId)
        val newLevel = question!!.level + 1
        updateAnswered(questionId, newLevel)
    }

    fun answeredIncorrect(questionId: Int) {
        val question = getQuestion(questionId)
        val newLevel = if (question!!.level <= 0) 0 else question.level - 1
        updateAnswered(questionId, newLevel)
    }

    fun continueNow(topicId: Int) {
        val updates = ContentValues()
        updates.put("next_time", Date().time)
        db!!.update(
            "question",
            updates,
            "topic_id=?",
            arrayOf(topicId.toString())
        )
    }

    fun resetTopic(topicId: Int) {
        val updates = ContentValues()
        updates.put("next_time", Date().time)
        updates.put("level", 0L)
        db!!.update(
            "question",
            updates,
            "topic_id=?",
            arrayOf(topicId.toString())
        )
    }

    fun setTopicsInSimpleCursorAdapter(adapter: SimpleCursorAdapter) {
        val c = getTopicsCursor(db)
        adapter.changeCursor(c)
    }

    private fun getMaxAnswerId(db: SQLiteDatabase): Int {
        val cursor =
            db.rawQuery("SELECT max(_id) AS _id FROM answer", arrayOf())
        cursor.moveToFirst()
        val result = cursor.getInt(0)
        cursor.close()
        return result
    }

    private fun getTopicsCursor(db: SQLiteDatabase?): Cursor {
        return db!!.rawQuery(
            "SELECT t._id AS _id, t.order_index AS order_index, t.name AS name, CASE WHEN MIN(level) >= $NUMBER_LEVELS THEN ? ELSE SUM(CASE WHEN level < $NUMBER_LEVELS THEN 1 ELSE 0 END) END AS status, MIN(CASE WHEN level >= $NUMBER_LEVELS THEN NULL ELSE next_time END) AS next_question FROM topic t LEFT JOIN question q ON q.topic_id = t._id GROUP BY t._id, t.order_index, t.name ORDER BY t.order_index",
            arrayOf(done)
        )
    }

    private fun updateAnswered(questionId: Int, newLevel: Int) {
        val newNextTime = Date().time + waitingTimeOnLevel(newLevel)
        val updates = ContentValues()
        updates.put("level", newLevel)
        updates.put("next_time", newNextTime)
        db!!.update(
            "question",
            updates,
            "_id=?",
            arrayOf(questionId.toString())
        )
    }

    private fun waitingTimeOnLevel(level: Int): Long {
        return if (level <= 0) 15000L else if (level == 1) 60000L else if (level == 2) 30 * 60000L else if (level == 3) 86400000L else if (level == 4) 3 * 86400000L else 0
    }

    private fun save(db: SQLiteDatabase, currentTopic: Topic?) {
        val contentValues = ContentValues()
        contentValues.put("_id", currentTopic!!.id)
        contentValues.put("order_index", currentTopic.index)
        contentValues.put("name", currentTopic.name)
        db.insert("topic", null, contentValues)
    }

    private fun save(db: SQLiteDatabase, question: Question?) {
        val contentValues = ContentValues()
        contentValues.put("_id", question!!.id)
        contentValues.put("topic_id", question.topicId)
        contentValues.put("reference", question.reference)
        contentValues.put("question", question.questionText)
        contentValues.put("level", 0)
        contentValues.put("next_time", question.nextTime!!.time)
        db.insert("question", null, contentValues)
        for ((answerIndex, answer) in question.answers.withIndex()) {
            contentValues.clear()
            contentValues.put("_id", ++answerIdSeq)
            contentValues.put("question_id", question.id)
            contentValues.put("order_index", answerIndex)
            contentValues.put("answer", answer)
            db.insert("answer", null, contentValues)
        }
    }

    private val db: SQLiteDatabase?
        get() {
            if (database == null) {
                database = this.writableDatabase
            }
            return database
        }

    override fun onUpgrade(
        db: SQLiteDatabase,
        oldVersion: Int,
        newVersion: Int
    ) {
        answerIdSeq = getMaxAnswerId(db)
        if (oldVersion <= 2) {
            val updates = ContentValues()
            updates.put(
                "question",
                "Welches Funkzeugnis ist mindestens erforderlich, um mit einer Seefunkstelle auf einem Sportfahrzeug am Weltweiten Seenot- und Sicherheitsfunksystem (GMDSS) im Seegebiet A3 teilnehmen zu können?"
            )
            db.update("question", updates, "_id=?", arrayOf("4408"))
        }
        if (oldVersion <= 3) {
            val updates = ContentValues()
            updates.put(
                "answer",
                "Sportboote ohne Antriebsmaschine oder solche mit einer größten nicht überschreitbaren Nutzleistung von 11,03 Kilowatt (15 PS) oder weniger."
            )
            db.update(
                "answer",
                updates,
                "question_id=? AND order_index=?",
                arrayOf("9664", "0")
            )
            updates.clear()
            updates.put(
                "answer",
                "Sportboote mit Antriebsmaschine mit einer größeren Nutzleistung als 11,03 Kilowatt (15 PS)."
            )
            db.update(
                "answer",
                updates,
                "question_id=? AND order_index=?",
                arrayOf("9664", "2")
            )
        }
        if (oldVersion <= 4) {
            updateQuestion(
                db,
                "9589",
                "Was ist zu tun, wenn vor Antritt der Fahrt nicht feststeht, wer Schiffsführer ist?"
            )
            updateAnswer(
                db,
                "9589",
                CORRECT_ANSWER,
                "Der verantwortliche Schiffsführer muss bestimmt werden."
            )
            updateAnswer(
                db,
                "9589",
                FIRST_INCORRECT,
                "Der verantwortliche Schiffsführer muss gewählt werden."
            )
            updateAnswer(
                db,
                "9589",
                SECOND_INCORRECT,
                "Ein Inhaber eines Sportbootführerscheins muss die "
                        + "Fahrzeugführung übernehmen."
            )
            updateAnswer(
                db,
                "9589",
                THIRD_INCORRECT,
                "Ein Inhaber eines Sportbootführerscheins muss die "
                        + "Verantwortung übernehmen."
            )
            updateAnswer(
                db,
                "9674",
                FIRST_INCORRECT,
                "Die Kollisionsverhütungsregeln (KVR), die "
                        + "Seeschifffahrtsstraßen-Ordnung (SeeSchStrO) und die "
                        + "Sportbootführerscheinverordnung."
            )
            updateAnswer(
                db,
                "9742",
                FIRST_INCORRECT,
                "Die Nachrichten für Seefahrer (NfS), herausgegeben vom "
                        + "Bundesamt für Seeschifffahrt und Hydrographie, sowie "
                        + "die Bekanntmachungen für Seefahrer (BfS) der örtlich "
                        + "zuständigen Wasserstraßen- und Schifffahrtsämter, die "
                        + "auf alle Veränderungen hinsichtlich Betonnung, Wracks "
                        + "und Untiefen sowie auf die Schifffahrt betreffende "
                        + "Maßnahmen und Ereignisse hinweisen."
            )
            updateAnswer(
                db,
                "9742",
                SECOND_INCORRECT,
                "Die nautische Veröffentlichung „Sicherheit auf dem "
                        + "Wasser, herausgegeben durch das Bundesministerium für "
                        + "Verkehr und digitale Infrastruktur (BMVI), mit "
                        + "wichtigen Regeln und Tipps für Wassersportler."
            )
            updateAnswer(
                db,
                "9743",
                SECOND_INCORRECT,
                "Ergänzende Vorschriften für den NOK in der "
                        + "Seeschifffahrtsstraßen-Ordnung sowie in der "
                        + "Sportbootführerscheinverordnung."
            )
            updateAnswer(
                db,
                "9814",
                SECOND_INCORRECT,
                "Befahrensregeln beachten sowie Wasserschutzpolizei und "
                        + "Wasserstraßen- und Schifffahrtsamt informieren."
            )
        }
        if (oldVersion <= 6) {
            if (oldVersion == 6) {
                deleteQuestion(db, 9999)
            }
            updateAnswer(
                db,
                "9676",
                CORRECT_ANSWER,
                "Es sind bekannt gemachte Schifffahrtswege, die durch "
                        + "Trennlinien oder Trennzonen in Einbahnwege geteilt sind."
            )
            val q264 = Question()
            q264.id = 9999
            q264.topicId = 9661
            q264.reference = "264"
            q264.questionText =
                "Was bedeuten die in der Wetterkarte abgebildeten Isobaren?"
            q264.answers.add("Orte gleichen Luftdrucks.")
            q264.answers.add("Orte gleicher Windstärke.")
            q264.answers.add("Orte gleicher Wolkenbildung.")
            q264.answers.add("Orte gleicher Luftfeuchtigkeit.")
            q264.nextTime = Date()
            save(db, q264)
        }

        if (oldVersion <= 7) {
            updateQuestion(
                db,
                "9626",
                "Wo finden Sie Informationen über umweltfreundliche Farben, Lacke und Antifouling-Beschichtungen für Ihr Boot?"
            )
            updateAnswer(
                db,
                "9626",
                CORRECT_ANSWER,
                "Beim Umweltbundesamt."
            )
            updateAnswer(
                db,
                "9626",
                FIRST_INCORRECT,
                "Beim Bundesministerium für Digitales und Verkehr."
            )
            updateAnswer(
                db,
                "9626",
                SECOND_INCORRECT,
                "In der Sportbootführerscheinverordnung."
            )
            updateAnswer(
                db,
                "9626",
                THIRD_INCORRECT,
                "In der Sportbootvermietungsverordnung."
            )

            updateQuestion(
                db,
                "9646",
                "Welche Veröffentlichungen enthalten wichtige Regeln und Tipps für Wassersportler, Empfehlungen zur Ausrüstung von Sportbooten sowie Hinweise zu umweltgerechtem Verhalten auf dem Wasser?"
            )
            updateAnswer(
                db,
                "9646",
                CORRECT_ANSWER,
                "Nautische Publikationen wie „Sicherheit auf dem Wasser” und „Sicher auf See”."
            )
            updateAnswer(
                db,
                "9646",
                FIRST_INCORRECT,
                "Verordnung über die Sicherung der Seefahrt und nautische Publikationen wie „Sicher auf See”."
            )
            updateAnswer(
                db,
                "9646",
                SECOND_INCORRECT,
                "Nautische Publikationen wie „Sicherheit auf dem Wasser” und Internationales Signalbuch."
            )
            updateAnswer(
                db,
                "9646",
                THIRD_INCORRECT,
                "Internationales Signalbuch und Verordnung über die Sicherung der Seefahrt."
            )

            updateQuestion(
                db,
                "9647",
                "Unter welchen Voraussetzungen darf ein Sportboot mit Elektromotor ohne Fahrerlaubnis geführt werden?"
            )
            updateAnswer(
                db,
                "9647",
                CORRECT_ANSWER,
                "Die Antriebsleistung beträgt höchstens 7,5 Kilowatt Betriebsart S1 (Dauerbetrieb)."
            )
            updateAnswer(
                db,
                "9647",
                FIRST_INCORRECT,
                "Es darf immer ohne Fahrerlaubnis geführt werden, unabhängig von der Antriebsleistung."
            )
            updateAnswer(
                db,
                "9647",
                SECOND_INCORRECT,
                "Bis zu einer Antriebsleistung von 11,03 Kilowatt Betriebsart S1 (Dauerbetrieb)."
            )
            updateAnswer(
                db,
                "9647",
                THIRD_INCORRECT,
                "Es darf nie ohne Fahrerlaubnis geführt werden, unabhängig von der Antriebsleistung."
            )

            updateAnswer(
                db,
                "9664",
                CORRECT_ANSWER,
                "Sportboote ohne Antriebsmaschine oder solche mit einer größten nicht überschreitbaren Nutzleistung von 11,03 Kilowatt (15 PS) bei Verwendung eines Verbrennungsmotors bzw. 7,5 Kilowatt bei Verwendung eines Elektromotors Betriebsart S1 (Dauerbetrieb) oder weniger."
            )
            updateAnswer(
                db,
                "9664",
                FIRST_INCORRECT,
                "Sportboote unter Segel mit einer Rumpflänge unter 20 m und solche deren Antriebsmaschine nicht benutzt wird."
            )
            updateAnswer(
                db,
                "9664",
                SECOND_INCORRECT,
                "Sportboote mit Antriebsmaschine mit einer größeren Nutzleistung als 11,03 Kilowatt (15 PS) bei Verwendung eines Verbrennungsmotors bzw. 7,5 Kilowatt bei Verwendung eines Elektromotors Betriebsart S1 (Dauerbetrieb)."
            )

            updateAnswer(
                db,
                "9667",
                CORRECT_ANSWER,
                "Der Fahrzeugführer hat die Besatzungsmitglieder und Gäste über die Sicherheitsvorkehrungen an Bord zu unterrichten, in die Handhabung der Rettungs- und Feuerlöschmittel einzuweisen und auf geeignete Maßnahmen gegen das Überbordfallen hinzuweisen, zudem darauf, dasss ständig angelegte Rettungswesten die Überlebenschancen im Wasser erhöhen."
            )
            updateAnswer(
                db,
                "9667",
                FIRST_INCORRECT,
                "Der Fahrzeugführer muss die Besatzungsmitglieder und Gäste anweisen, dass sie sich über die Sicherheitsvorkehrungen an Bord informieren, sich die Gebrauchsanweisungen der Rettungs- und Feuerlöschmittel ansehen und auf geeignete Maßnahmen gegen das Überbordfallen achten, zudem darauf, dass ständig angelegte Rettungswesten die Überlebenschancen im Wasser erhöhen."
            )
            updateAnswer(
                db,
                "9667",
                SECOND_INCORRECT,
                "Der Fahrzeugführer hat die verantwortlichen Besatzungsmitglieder über die Sicherheitsvorkehrungen an Bord zu unterrichten, in die Handhabung der Rettungs- und Feuerlöschmittel einzuweisen und auf geeignete Maßnahmen gegen das Überbordfallen hinzuweisen, zudem darauf, dass ständig angelegte Rettungswesten die Überlebenschancen im Wasser erhöhen."
            )
            updateAnswer(
                db,
                "9667",
                THIRD_INCORRECT,
                "Der Fahrzeugführer hat die Gäste an Bord über die Sicherheitsvorkehrungen an Bord zu unterrichten, in die Handhabung der Rettungs- und Feuerlöschmittel einzuweisen und auf geeignete Maßnahmen gegen das Überbordfallen hinzuweisen, zudem darauf, dass ständig angelegte Rettungswesten die Überlebenschancen im Wasser erhöhen."
            )

            updateAnswer(
                db,
                "9733",
                THIRD_INCORRECT,
                "In den „Bekanntmachungen der Generaldirektion Wasserstraßen und Schifffahrt (GDWS)”."
            )

            updateAnswer(
                db,
                "9742",
                CORRECT_ANSWER,
                "Die Bekanntmachungen der Generaldirektion Wasserstraßen- und Schifffahrt (GDWS), die besondere örtliche Regelungen enthalten und Hinweise für die einzelnen Seeschifffahrtsstraßen geben."
            )
            updateAnswer(
                db,
                "9742",
                SECOND_INCORRECT,
                "Die Bekanntmachung der Generaldirektion Wasserstraßen- und Seeschifffahrt (GDWS) sowie die nautische Veröffentlichung „Sicherheit auf dem Wasser”, herausgegeben durch das Bundesministerium für Digitales und Verkehr (BMDV), mit wichtigen Regeln und Tipps für Wassersportler."
            )

            updateAnswer(
                db,
                "9758",
                CORRECT_ANSWER,
                "Außerhalb des Fahrwassers, wenn es nicht von der Generaldirektion Wasserstraßen- und Schifffahrt (GDWS) durch Bekanntmachung verboten ist. Im Fahrwasser auf Abschnitten, die durch die GDWS bekanntgemacht oder durch blaue Tafeln mit dem weißen Symbol eines Wasserskiläufers, eines Wassermotrrades oder eines Segelsurfers bezeichnet sind."
            )
            updateAnswer(
                db,
                "9758",
                FIRST_INCORRECT,
                "Außerhalb der Seeschifffahrtsstraße, wenn es nicht von der Generaldirektion Wasserstraßen- und Schifffahrt (GDWS) durch Bekanntmachung verboten ist. Innerhalb der Seeschifffahrtsstraße auf Abschnitten, die durch die GDWS bekanntgemacht oder durch blaue Tafeln mit dem weißen Symbol eines Wasserskiläufers, eines Wassermotorrades oder eines Segelsurfers bezeichnet sind."
            )
            updateAnswer(
                db,
                "9758",
                THIRD_INCORRECT,
                "Im Fahrwasser, wenn es nicht von der Generaldirektion Wasserstraßen- und Schifffahrt (GDWS) durch Bekanntmachung verboten ist. Außerhalb des Fahrwassers auf Abschnitten, die durch die GDWS bekanntgemacht oder durch blaue Tafeln mit dem weißen Symbol eines Wasserskiläufers, eines Wassermotorrades oder eines Segelsurfers bezeichnet sind."
            )

            updateAnswer(
                db,
                "9760",
                THIRD_INCORRECT,
                "Im Fahrwasser, wenn es durch die Generaldirektion Wasserstraßen- und Schifffahrt (GDWS) bekanntgemacht worden ist. Außerhalb des Fahrwassers auf Abschnitten, die durch die GDWS bekanntgemacht oder durch entsprechendes Sichtzeichen bezeichnet sind."
            )

            updateAnswer(
                db,
                "9763",
                CORRECT_ANSWER,
                "Ergänzende Vorschriften für den NOK in der Seeschifffahrtsstraßen-Ordnung sowie in den Bekanntmachungen der Generaldirektion Wasserstraßen und Schifffahrt (GDWS)."
            )

            updateAnswer(
                db,
                "9772",
                CORRECT_ANSWER,
                "An bestimmten Tag- und Nachtsignalen, die nach der Schifffahrtspolizeiverordnung der Generaldirektion Wasserstraßen- und Schifffahrt (GDWS) für militärische Sperr- und Warngebiete an entsprechenden Signalstellen und auf Sicherungsfahrzeugen gezeigt werden."
            )
            updateAnswer(
                db,
                "9772",
                FIRST_INCORRECT,
                "An bestimmten Tag- und Nachtsignalen, die nach der Rheinpolizeiverordnung der Generaldirektion Wasserstraßen- und Schifffahrt (GDWS) für militärische Sperr- und Warngebiete an entsprechenden Signalstellen am Ufer und auf Sicherungsfahrzeugen gezeigt werden."
            )
            updateAnswer(
                db,
                "9772",
                THIRD_INCORRECT,
                "An bestimmten Tag- und Nachtsignalen, die nach der Schifffahrtsordnung der Generaldirektion Wasserstraßen- und Schifffahrt (GDWS) für militärische Sperr- und Warngebiete an entsprechenden Signalstellen am Ufer und auf Sicherungsfahrzeugen gezeigt werden."
            )

            updateAnswer(
                db,
                "9814",
                CORRECT_ANSWER,
                "Befahrensregelungen beachten."
            )

            updateAnswer(
                db,
                "9815",
                CORRECT_ANSWER,
                "Befahrensverbote, Befahrensbeschränkungen, Geschwindigkeitsbeschränkungen, besondere Regelungen für das Wasserskilaufen, das Fahren mit Wassermotorrädern und das Segelsurfen."
            )
            updateAnswer(
                db,
                "9815",
                SECOND_INCORRECT,
                "Befahrensverbote, Befahrensbeschränkungen, Mindestgeschwindigkeiten, besondere Regelungen für das Befahren von Verkehrstrennungsgebieten."
            )

            updateQuestion(
                db,
                "9816",
                "Welche Verkehre können in einer ausgewiesenen Erlaubniszone zugelassen werden?"
            )
            updateAnswer(
                db,
                "9816",
                CORRECT_ANSWER,
                "Bestimmte Wassersportgeräte."
            )
            updateAnswer(
                db,
                "9816",
                FIRST_INCORRECT,
                "Bestimmte Fischereifahrzeuge."
            )
            updateAnswer(
                db,
                "9816",
                SECOND_INCORRECT,
                "Bestimmte Bodeneffekt- und Luftkissenfahrzeuge."
            )
            updateAnswer(
                db,
                "9816",
                THIRD_INCORRECT,
                "Bestimmte Arbeitsgeräte für die Erkundung fossiler Brennstoffe"
            )

            updateQuestion(
                db,
                "9817",
                "Was verstehen Sie gemäß Nordsee-Befahrens-Verordnung (NordSBefV) unter Schnellfahrkorridoren?"
            )
            updateAnswer(
                db,
                "9817",
                CORRECT_ANSWER,
                "Ausgewiesene Wasserflächen für den gewerblichen Verkehr."
            )
            updateAnswer(
                db,
                "9817",
                FIRST_INCORRECT,
                "Ausgewiesene Wasserflächen für bestimmte Sportverkehre."
            )
            updateAnswer(
                db,
                "9817",
                SECOND_INCORRECT,
                "Wasserflächen zum Starten und Landen von Wasserflugzeugen."
            )
            updateAnswer(
                db,
                "9817",
                THIRD_INCORRECT,
                "Wasserflächen, von denen Taucher 500 m Abstand halten müssen."
            )

            updateQuestion(
                db,
                "9818",
                "Wie hoch, soweit die Nordsee-Befahrens-Verordnung (NordSBefV) nicht ausdrücklich etwas anderes bestimmt, ist die maximale Geschwindigkeit, die ein Maschinenfahrzeug in Nationalparken im Bereich der Nordsee fahren darf?"
            )
            updateAnswer(
                db,
                "9818",
                CORRECT_ANSWER,
                "12 Knoten über Grund"
            )
            updateAnswer(
                db,
                "9818",
                FIRST_INCORRECT,
                "12 Knoten Fahrt durchs Wasser."
            )
            updateAnswer(
                db,
                "9818",
                SECOND_INCORRECT,
                "10 Knoten Fahrt über Grund."
            )
            updateAnswer(
                db,
                "9818",
                THIRD_INCORRECT,
                "10 Knoten Fahrt durchs Wasser."
            )

            updateAnswer(
                db,
                "9820",
                CORRECT_ANSWER,
                "Seekarten, Leuchtfeuerverzeichnis, Seehandbücher, Gezeitentafeln oder -kalender, Funkdienst für die Klein- und Sportschifffahrt, Nachrichten für Seefahrer (NfS), Bekanntmachungen für Seefahrer (BfS)."
            )
            updateAnswer(
                db,
                "9820",
                FIRST_INCORRECT,
                "Seekarten, Verordnung über die Sicherung der Seefahrt, Seehandbücher, Gezeitentafeln oder -kalender, Funkdienst für die Klein- und Sportschifffahrt, Nachrichten für Seefahrer (NfS)."
            )
            updateAnswer(
                db,
                "9820",
                SECOND_INCORRECT,
                "Seeschifffahrtsstraßen-Ordnung, Leuchtfeuerverzeichnis, Seehandbücher, Verordnung über die Sicherung der Seefahrt, Gezeitentafeln oder -kalender, Bekanntmachungen der Generaldirektion Wasserstraßen- und Schifffahrt (GDWS)."
            )
            updateAnswer(
                db,
                "9820",
                THIRD_INCORRECT,
                "Schifffahrtspolizeiliche Anordnungen, Gezeitentafeln oder -kalender, Funkdienst für die Klein- und Sportschifffahrt, Nachrichten für Seefahrer (NfS), Bekanntmachungen für Seefahrer (BfS)."
            )

            updateQuestion(
                db,
                "9822",
                "Welchen Effekt können Wind und gegenläufiger Tidenstrom im Bereich von Seegaten haben?"
            )
            updateAnswer(
                db,
                "9822",
                CORRECT_ANSWER,
                "Steile und aufbäumende Seen (Brecher)."
            )
            updateAnswer(
                db,
                "9822",
                FIRST_INCORRECT,
                "Der Tidenstrom wird durch den Wind verstärkt."
            )
            updateAnswer(
                db,
                "9822",
                SECOND_INCORRECT,
                "Keinen."
            )
            updateAnswer(
                db,
                "9822",
                THIRD_INCORRECT,
                "Der Tidenstrom glättet die Windsee."
            )

            updateAnswer(
                db,
                "9862",
                CORRECT_ANSWER,
                "Insbesondere alle Navigationsanlagen sorgfältig gebrauchen, die Sichtbarkeit des eigenen Fahrzeugs erhöhen (z.B. Radarreflektor, AIS) und in einem Revier mit Landradarberatung die Radarberatung über UKW-Sprechfunk mithören."
            )
            updateAnswer(
                db,
                "9862",
                SECOND_INCORRECT,
                "Insbesondere alle Navigationsanlagen sorgfältig gebrauchen, die Sichtbarkeit des eigenen Fahrzeugs erhöhen (z.B. Radarreflektor, AIS) und die Verkehrszentrale ständig über Kurs und Geschwindigkeit informieren."
            )

            updateAnswer(
                db,
                "9863",
                CORRECT_ANSWER,
                "Verschlusszustand herbeiführen, lose Gegenstände festzurren, Rettungsweste und andere Rettungsmittel bereithalten; wenn erforderlich und möglich, Schutzhafen anlaufen."
            )
            updateAnswer(
                db,
                "9863",
                FIRST_INCORRECT,
                "Verschlusszustand herbeiführen, lose Gegenstände festzurren, Rettungsweste und andere Rettungsmittel bereithalten, Seenotsignalmittel zum Einsatz vorbereiten."
            )
            updateAnswer(
                db,
                "9863",
                SECOND_INCORRECT,
                "Türen schließen, lose Gegenstände festzurren, Rettungsweste und andere Rettungsmittel bereithalten, Radar, Ruder und UKW besetzen."
            )
            updateAnswer(
                db,
                "9863",
                THIRD_INCORRECT,
                "Türen schließen, lose Gegenstände festzurren, Rettungsweste und andere Rettungsmittel bereithalten, Seenotsignalmittel zum Einsatz vorbereiten."
            )
        }
    }

    private fun updateQuestion(
        db: SQLiteDatabase,
        id: String,
        text: String
    ) {
        val update = ContentValues()
        update.put(QUESTION, text)
        db.update(QUESTION, update, QUESTION_QUERY, arrayOf(id))
    }

    private fun updateAnswer(
        db: SQLiteDatabase,
        question: String,
        answerNo: String,
        text: String
    ) {
        val upd = ContentValues()
        upd.put(ANSWER, text)
        db.update(ANSWER, upd, ANSWER_QUERY, arrayOf(question, answerNo))
    }

    private fun deleteQuestion(
        db: SQLiteDatabase,
        id: Int
    ) {
        db.delete(QUESTION, QUESTION_QUERY, arrayOf(id.toString()))
    }

    companion object {
        private const val QUESTION = "question"
        private const val QUESTION_QUERY = "_id=?"
        private const val ANSWER = "answer"
        private const val ANSWER_QUERY = "question_id=? AND order_index=?"
        private const val CORRECT_ANSWER = "0"
        private const val FIRST_INCORRECT = "1"
        private const val SECOND_INCORRECT = "2"
        private const val THIRD_INCORRECT = "3"
        private const val NUMBER_LEVELS = 5
    }

}