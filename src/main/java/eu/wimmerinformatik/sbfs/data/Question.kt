/*  Copyright 2014 - 2022 Matthias Wimmer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package eu.wimmerinformatik.sbfs.data

import java.util.*

class Question {
    var id = 0
    var topicId = 0
    var reference: String? = null
    var questionText: String? = null
    var answers: MutableList<String> = mutableListOf()
    var level = 0
    var nextTime: Date? = null
}