/*  Copyright 2014 - 2022 Matthias Wimmer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package eu.wimmerinformatik.sbfs

import android.app.Activity
import eu.wimmerinformatik.sbfs.data.Repository
import android.os.Bundle
import android.widget.ListView
import android.widget.SimpleCursorAdapter
import android.view.View
import android.database.Cursor
import android.widget.TextView
import android.widget.AdapterView.OnItemClickListener
import android.widget.AdapterView
import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import android.net.Uri
import java.text.DateFormat
import java.util.*

class SBFSTrainerActivity : Activity() {
    private var repository: Repository? = null

    /**
     * Called when the activity is first created.
     */
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)
        repository = Repository(this)
    }

    public override fun onResume() {
        super.onResume()
        val topicList = findViewById<ListView>(R.id.listView1)
        val adapter = SimpleCursorAdapter(
            this,
            R.layout.topic_list_item,
            null,
            arrayOf("name", "status", "next_question"),
            intArrayOf(
                R.id.topicListItem,
                R.id.topicStatusView,
                R.id.nextQuestionTime
            )
        )
        adapter.viewBinder =
            SimpleCursorAdapter.ViewBinder { view: View, cursor: Cursor, columnIndex: Int ->
                if (columnIndex == 4) {
                    val textView = view as TextView
                    if (!cursor.isNull(4)) {
                        val nextQuestion = cursor.getLong(4)
                        val now = Date().time
                        if (nextQuestion > now) {
                            textView.text =
                                getString(
                                    R.string.nextLabel,
                                    (if (nextQuestion - now < 64800000L) DateFormat.getInstance() else DateFormat.getDateTimeInstance())
                                        .format(Date(nextQuestion))
                                )
                        }
                    } else {
                        textView.text = ""
                    }
                }
                columnIndex == 4
            }
        topicList.adapter = adapter
        repository!!.setTopicsInSimpleCursorAdapter(adapter)

        //@Override
        topicList.onItemClickListener =
            OnItemClickListener { _: AdapterView<*>?, _: View?, _: Int, id: Long ->
                val intent =
                    Intent(this@SBFSTrainerActivity, QuestionAsker::class.java)
                intent.putExtra(QuestionAsker::class.java.name + ".topic", id)
                startActivity(intent)
            }
    }

    public override fun onPause() {
        super.onPause()
        val topicList = findViewById<ListView>(R.id.listView1)
        val adapter = topicList.adapter as SimpleCursorAdapter
        val previousCursor = adapter.cursor
        adapter.changeCursor(null)
        previousCursor.close()
        topicList.adapter = null
    }

    /**
     * Populate options menu.
     *
     * @param menu the menu to populate
     * @return always true
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.mainmenu, menu)
        return true
    }

    /**
     * Handle option menu selections.
     *
     * @param item the Item the user selected
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle item selection
        return when (item.itemId) {
            R.id.mainHelp -> {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data =
                    Uri.parse("http://sportboot.mobi/trainer/segeln/sbfs/app/help?view=TrainerActivity")
                startActivity(intent)
                true
            }
            R.id.mainInfo -> {
                val intent2 = Intent(Intent.ACTION_VIEW)
                intent2.data =
                    Uri.parse("http://sportboot.mobi/trainer/segeln/sbfs/app/info?view=TrainerActivity")
                startActivity(intent2)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}