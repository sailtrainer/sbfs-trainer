/*  Copyright 2014 - 2022 Matthias Wimmer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package eu.wimmerinformatik.sbfs

import android.app.Activity
import eu.wimmerinformatik.sbfs.data.Repository
import android.os.Bundle
import android.widget.TextView
import android.widget.ProgressBar
import android.view.Menu
import android.view.MenuItem
import android.content.Intent
import android.net.Uri

class StatisticsActivity : Activity() {
    private var repository: Repository? = null
    private var topicId = 0
    public override fun onDestroy() {
        super.onDestroy()
        repository!!.close()
        repository = null
    }

    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putLong(javaClass.name + ".topic", topicId.toLong())
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        repository = Repository(this)
        topicId =
            savedInstanceState?.getLong(javaClass.name + ".topic")?.toInt()
                ?: intent.extras!!.getInt(javaClass.name + ".topic")
        setContentView(R.layout.statistics)
        val topicName = findViewById<TextView>(R.id.topicName)
        topicName.text = repository!!.getTopic(topicId)!!.name
        val stats = repository!!.getTopicStat(topicId)
        val totalProgress = findViewById<ProgressBar>(R.id.totalProgress)
        totalProgress.max = stats.maxProgress
        totalProgress.progress = stats.currentProgress
        val atLevel0 = findViewById<ProgressBar>(R.id.atLevel0)
        atLevel0.max = stats.questionCount
        atLevel0.progress = stats.questionsAtLevel[0]
        val atLevel1 = findViewById<ProgressBar>(R.id.atLevel1)
        atLevel1.max = stats.questionCount
        atLevel1.progress = stats.questionsAtLevel[1]
        val atLevel2 = findViewById<ProgressBar>(R.id.atLevel2)
        atLevel2.max = stats.questionCount
        atLevel2.progress = stats.questionsAtLevel[2]
        val atLevel3 = findViewById<ProgressBar>(R.id.atLevel3)
        atLevel3.max = stats.questionCount
        atLevel3.progress = stats.questionsAtLevel[3]
        val atLevel4 = findViewById<ProgressBar>(R.id.atLevel4)
        atLevel4.max = stats.questionCount
        atLevel4.progress = stats.questionsAtLevel[4]
        val atLevel5 = findViewById<ProgressBar>(R.id.atLevel5)
        atLevel5.max = stats.questionCount
        atLevel5.progress = stats.questionsAtLevel[5]
    }

    /**
     * Populate the options menu.
     *
     * @param menu the menu to populate
     * @return always true
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.statisticsmenu, menu)
        return true
    }

    /**
     * Handle option menu selections.
     *
     * @param item the Item the user selected
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle item selection
        if (item.itemId == R.id.statHelp) {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data =
                Uri.parse("http://sportboot.mobi/trainer/segeln/sbfs/app/help?view=StatisticsActivity")
            startActivity(intent)
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}